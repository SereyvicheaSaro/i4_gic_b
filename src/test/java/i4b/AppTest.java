package i4b;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    
    @Test
    public void addNumTest() // New test method
    {
        App app = new App();
        int result = app.addNum(1, 2);
        assertTrue(result == 3);
    }
}
